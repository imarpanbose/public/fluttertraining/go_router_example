import 'package:go_router/go_router.dart';
import 'package:router_example/error_page.dart';
import 'package:router_example/home.dart';
import 'package:router_example/profile.dart';
import 'package:router_example/test.dart';

class MyRouter{

  route(){
    return GoRouter(
      initialLocation: '/',
      routes: [
        GoRoute(
          path: '/',
          builder: (constext,state)=>const HomePage(),
        ),
        GoRoute(
          name: "profile",
          path: '/profile',
          builder: (constext,state)=>ProfilePage(id: state.uri.queryParameters['name']!),
          
        ),
        GoRoute(
          name: "test",
          path: '/test',
          builder: (constext,state)=>const TestPage(),

          
        ),
      ],
      errorBuilder: ((context, state) => ErrorPage(errorText: state.error!.message)),
      redirect: (context, state) {
            print(state.fullPath);
            bool isAuth=true;
            return !isAuth?'/login':null;
          },
    );
  }

}