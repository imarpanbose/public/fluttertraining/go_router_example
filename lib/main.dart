import 'package:flutter/material.dart';
import 'package:router_example/my_router.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  MyRouter myRouter=MyRouter();
   MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: myRouter.route(),
    );
  }
}
